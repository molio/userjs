// This file is CC0: http://creativecommons.org/publicdomain/zero/1.0/
// Firefox License: https://www.mozilla.org/about/legal/terms/firefox/
// last update: 2021-05-15
// modified (comment): 2022-02-20
// user_pref("設定名","値");
// 値について、数値(0,1,2..)と真偽値(true, false)に対してはダブルクオテーション("")で括ってはいけません。
// 参考：http://kiz.sinumade.net/config/firefox-userjs

// *********** フォント
// *********** ブラウジング・コンテンツ
// *********** 機能の無效化
// *********** アドレスバー
// *********** 檢索エンジン
// *********** キャッシュ
// *********** 接續數
// *********** 履歷
// *********** ダウンロード
// *********** フォルダ
// *********** タブ・ウィンドウ
// *********** セキュリティ
// *********** 警吿各種
// *********** Firefox システム
// *********** 外部ソフトウェア

// ***************************** フォント
// プロポーショナル、既定のフォント sans-serif か serif か
user_pref("font.default.ja", "serif");
user_pref("font.default.x-western", "sans-serif");
// プロポーショナル、既定のフォントのサイズ
user_pref("font.size.variable.ar", 18);
user_pref("font.size.variable.el", 18);
user_pref("font.size.variable.he", 18);
user_pref("font.size.variable.ja", 18);
user_pref("font.size.variable.ko", 18);
user_pref("font.size.variable.th", 18);
user_pref("font.size.variable.tr", 18);
user_pref("font.size.variable.x-armn", 18);
user_pref("font.size.variable.x-baltic", 18);
user_pref("font.size.variable.x-beng", 18);
user_pref("font.size.variable.x-cans", 18);
user_pref("font.size.variable.x-central-euro", 18);
user_pref("font.size.variable.x-cyrillic", 18);
user_pref("font.size.variable.x-devanagari", 18);
user_pref("font.size.variable.x-ethi", 18);
user_pref("font.size.variable.x-geor", 18);
user_pref("font.size.variable.x-gujr", 18);
user_pref("font.size.variable.x-guru", 18);
user_pref("font.size.variable.x-khmr", 18);
user_pref("font.size.variable.x-knda", 18);
user_pref("font.size.variable.x-mlym", 18);
user_pref("font.size.variable.x-orya", 18);
user_pref("font.size.variable.x-sinh", 18);
user_pref("font.size.variable.x-tamil", 18);
user_pref("font.size.variable.x-telu", 18);
user_pref("font.size.variable.x-tibt", 18);
user_pref("font.size.variable.x-unicode", 18);
user_pref("font.size.variable.x-user-def", 18);
user_pref("font.size.variable.x-western", 18);
user_pref("font.size.variable.x-x-western", 18);
user_pref("font.size.variable.zh-CN", 18);
user_pref("font.size.variable.zh-HK", 18);
user_pref("font.size.variable.zh-TW", 18);
// 等幅 (Monospace) のサイズ
user_pref("font.size.fixed.ar", 18);
user_pref("font.size.fixed.el", 18);
user_pref("font.size.fixed.he", 18);
user_pref("font.size.fixed.ja", 18);
user_pref("font.size.fixed.ko", 18);
user_pref("font.size.fixed.th", 18);
user_pref("font.size.fixed.tr", 18);
user_pref("font.size.fixed.x-armn", 18);
user_pref("font.size.fixed.x-baltic", 18);
user_pref("font.size.fixed.x-beng", 18);
user_pref("font.size.fixed.x-cans", 18);
user_pref("font.size.fixed.x-central-euro", 18);
user_pref("font.size.fixed.x-cyrillic", 18);
user_pref("font.size.fixed.x-devanagari", 18);
user_pref("font.size.fixed.x-ethi", 18);
user_pref("font.size.fixed.x-geor", 18);
user_pref("font.size.fixed.x-gujr", 18);
user_pref("font.size.fixed.x-guru", 18);
user_pref("font.size.fixed.x-khmr", 18);
user_pref("font.size.fixed.x-knda", 18);
user_pref("font.size.fixed.x-mlym", 18);
user_pref("font.size.fixed.x-orya", 18);
user_pref("font.size.fixed.x-sinh", 18);
user_pref("font.size.fixed.x-tamil", 18);
user_pref("font.size.fixed.x-telu", 18);
user_pref("font.size.fixed.x-tibt", 18);
user_pref("font.size.fixed.x-unicode", 18);
user_pref("font.size.fixed.x-user-def", 18);
user_pref("font.size.fixed.x-western", 18);
user_pref("font.size.fixed.x-x-western", 18);
user_pref("font.size.fixed.zh-CN", 18);
user_pref("font.size.fixed.zh-HK", 18);
user_pref("font.size.fixed.zh-TW", 18);
// 最小フォントサイズ
user_pref("font.minimum-size.ar", 12);
user_pref("font.minimum-size.el", 12);
user_pref("font.minimum-size.he", 12);
user_pref("font.minimum-size.ja", 12);
user_pref("font.minimum-size.ko", 12);
user_pref("font.minimum-size.th", 12);
user_pref("font.minimum-size.tr", 12);
user_pref("font.minimum-size.x-armn", 12);
user_pref("font.minimum-size.x-baltic", 12);
user_pref("font.minimum-size.x-beng", 12);
user_pref("font.minimum-size.x-cans", 12);
user_pref("font.minimum-size.x-central-euro", 12);
user_pref("font.minimum-size.x-cyrillic", 12);
user_pref("font.minimum-size.x-devanagari", 12);
user_pref("font.minimum-size.x-ethi", 12);
user_pref("font.minimum-size.x-geor", 12);
user_pref("font.minimum-size.x-gujr", 12);
user_pref("font.minimum-size.x-guru", 12);
user_pref("font.minimum-size.x-khmr", 12);
user_pref("font.minimum-size.x-knda", 12);
user_pref("font.minimum-size.x-mlym", 12);
user_pref("font.minimum-size.x-orya", 12);
user_pref("font.minimum-size.x-sinh", 12);
user_pref("font.minimum-size.x-tamil", 12);
user_pref("font.minimum-size.x-telu", 12);
user_pref("font.minimum-size.x-tibt", 12);
user_pref("font.minimum-size.x-unicode", 12);
user_pref("font.minimum-size.x-user-def", 12);
user_pref("font.minimum-size.x-western", 12);
user_pref("font.minimum-size.x-x-western", 12);
user_pref("font.minimum-size.zh-CN", 12);
user_pref("font.minimum-size.zh-HK", 12);
user_pref("font.minimum-size.zh-TW", 12);


// ***************************** ブラウジング・コンテンツ
// アニメーション
user_pref("image.animation_mode", "once"); // GIFアニメ
// Javascript による變更を無效
user_pref("dom.event.clipboardevents.enabled", false);
user_pref("dom.event.contextmenu.enabled", false);
user_pref("dom.disable_window_move_resize", true);
user_pref("dom.disable_window_open_feature.close", true);
user_pref("dom.disable_window_open_feature.location", true);
user_pref("dom.disable_window_open_feature.menubar", true);
user_pref("dom.disable_window_open_feature.minimizable", true);
user_pref("dom.disable_window_open_feature.personalbar", true);
user_pref("dom.disable_window_open_feature.resizable", true);
user_pref("dom.disable_window_open_feature.status", true);
user_pref("dom.disable_window_open_feature.titlebar", true);
user_pref("dom.disable_window_open_feature.toolbar", true);


// ***************************** 機能の無效化
// リンクの先讀み機能を無效
user_pref("network.prefetch-next", false);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.dns.disablePrefetchFromHTTPS", true); // nothing
// ネットワーク豫測機能を無效
user_pref("network.predictor.enabled", false);
user_pref("network.predictor.enable-prefetch", false);
// リファラを無效
// 0: 無效, 2: 有效
user_pref("network.http.sendRefererHeader", 0);
user_pref("network.http.referer.spoofSource", false);
// 完全なホスト名が一致する場合にのみRefererヘッダを送信する
// 0: 送信, 1: 同じeTLDのサイトに送信, 2: 完全なホスト名が一致する場合にのみ送信
user_pref("network.http.referer.XOriginPolicy", 2);
// オリジンをまたいで Referer を送信する場合、クロスオリジンリクエストの Referer ヘッダーには、scheme、host、port のみを送信する
// 0: 完全なURLを送信, 1: クエリ文字列を含まないURLを送信, 2: スキーム、ホスト、およびポートのみを送信
user_pref("network.http.referer.XOriginTrimmingPolicy", 2);
// 位置情報を無效
user_pref("geo.enabled", false);
user_pref("geo.wifi.uri", "");
user_pref("browser.search.geoip.url", "");
// 位置情報に基づいた檢索を無效
user_pref("browser.search.geoSpecificDefaults", false);
user_pref("browser.search.geoSpecificDefaults.url", "");
// Pocket を無效
user_pref("extensions.pocket.enabled", false);
user_pref("extensions.pocket.api", "");
user_pref("extensions.pocket.site", "");
user_pref("reader.parse-on-load.enabled", false);
// Web Push を無效
user_pref("dom.push.enabled", false);
user_pref("dom.push.connection.enabled", false);
// オートスクロールを無效
user_pref("general.autoScroll", false);
// スムーススクロールを無效
user_pref("general.smoothScroll", false);
user_pref("toolkit.scrollbox.smoothScroll", false);
// スペルチェックを無效
user_pref("layout.spellcheckDefault", 0);
// WebRTC を無效
user_pref("media.peerconnection.enabled", false);
// ウェブビーコンを無效
user_pref("beacon.enabled", false);
// Activity Stream を無效
user_pref("browser.library.activity-stream.enabled", false);
// Firefox Screenshots を無效
user_pref("extensions.screenshots.upload-disabled", false);
user_pref("extensions.screenshots.disabled", true);
// Ping 送信を無效
user_pref("browser.send_pings", false);
user_pref("browser.send_pings.max_per_link", 0);
user_pref("browser.send_pings.require_same_host", true);
// Telemetry を無效
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.ping-centre.telemetry", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);
user_pref("toolkit.telemetry.cachedClientID", "");
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
user_pref("toolkit.telemetry.hybridContent.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("toolkit.telemetry.server", "");
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
// Firefox ホームコンテンツ を無效
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.prerender", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeBookmarks", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeDownloads", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeVisited", false);
user_pref("browser.newtabpage.activity-stream.showSearch", false);
// おすすめの拡張機能を紹介する を無效
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
// おすすめの機能を紹介する を無效
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
// 必要であればタッチキーボードを表示する を無效
user_pref("ui.osk.enabled", false);
// ピクチャーインピクチャーの動画の操作 を無效
user_pref("media.videocontrols.picture-in-picture.video-toggle.enabled", false);
// HTTPS-Only モード を無效
user_pref("dom.security.https_only_mode_pbm", false);
// WebGL を無效
user_pref("webgl.disabled", true);


// ***************************** アドレスバー
// URL の省略を無效
user_pref("browser.urlbar.trimURLs", false);
// URL のハイライトを無效
user_pref("browser.urlbar.formatting.enabled", false);
// アドレスバーで提案するURLの數
user_pref("browser.urlbar.maxRichResults", 10);
// アドレスバー使用時に表示する候補
user_pref("browser.urlbar.suggest.bookmark", true);
user_pref("browser.urlbar.suggest.history", true);
user_pref("browser.urlbar.suggest.openpage", false); // 開いてゐるタブ
user_pref("browser.urlbar.suggest.engines", false); // 検索エンジン
user_pref("browser.urlbar.suggest.topsites", false); // トップサイト
// 檢索候補を無效
user_pref("browser.urlbar.suggest.searches", false);


// ***************************** 檢索エンジン
// 檢索候補の使用を無效
user_pref("browser.search.suggest.enabled", false);


// ***************************** キャッシュ
// SSL接續のコンテンツのキャッシュを無效
user_pref("browser.cache.disk_cache_ssl", false);
// オフライン用のキャッシュを無效
user_pref("browser.cache.offline.enable", false);


// ***************************** 履歷
// 戾ると進むで移動出來る最大ページ數
user_pref("browser.sessionhistory.max_entries", 20);


// ***************************** フォルダ
// 常に特定のフォルダにダウンロードする
user_pref("browser.download.useDownloadDir", true);
// 最後に開いたファイルのフォルダ
user_pref("browser.open.lastDir", "");
// 最後に保存したファイルのフォルダ
user_pref("browser.download.dir", "Downloads");


// ***************************** タブ・ウィンドウ
// 最後のタブを閉ぢても、ウィンドウを閉ぢない
user_pref("browser.tabs.closeWindowWithLastTab", false);
// タイトルバーにタブを描畫しない
user_pref("browser.tabs.drawInTitlebar", false);
// 起動時に開くページ
// 0: 空白, 1: ホームページ, 2: 最後に開いたページ, 3: 前回のセッションを復元
user_pref("browser.startup.page", 0);
// ホームページ
user_pref("browser.startup.homepage", "about:blank");
// 新しいタブに開くページ
user_pref("browser.newtab.url", "about:blank");
// 新しいタブ關聯
user_pref("browser.newtab.preload", false);
user_pref("browser.newtabpage.enabled", false);
// タブが選擇されるまで讀込みを待つ
user_pref("browser.sessionstore.restore_on_demand", true);
// リンクを新しいタブで開いた時、フォーカスする
user_pref("browser.tabs.loadInBackground", false);
// 新しいタブを現在のタブの後に插入する
user_pref("browser.tabs.insertAfterCurrent", true);
// 關聯するタブを現在のタブの後に插入する
user_pref("browser.tabs.insertRelatedAfterCurrent", true);
// タブを閉ぢた時に、タブを開いた元のタブにフォーカスする
user_pref("browser.tabs.selectOwnerOnClose", true);
// ポップアップをブロックする
user_pref("dom.disable_open_during_load", true);
// アドレスバーからの入力（檢索含む）を新しいタブで開く
user_pref("browser.urlbar.openintab", true);
// 檢索バーからの檢索結果を新しいタブで開く
user_pref("browser.search.openintab", true);
// ブックマークを新しいタブで開く
user_pref("browser.tabs.loadBookmarksInTabs", true);
// 新しいウィンドウで開くリンクの開き方
// 1: 現在のタブ, 2: 新しいウィンドウ, 3: 新しいタブ
user_pref("browser.link.open_newwindow", 1);
// 外部アプリケーションによるリンクの開き方
// -1: デフォルト, 1: 現在のタブ, 2: 新しいウィンドウ, 3: 新しいタブ
user_pref("browser.link.open_newwindow.override.external", 3);
// JavaScript によるポップアップの扱ひ
// 0: タブで開く, 1: ポップアップを許可する, 2: ポップアップのリサイズを許可する
user_pref("browser.link.open_newwindow.restriction", 0);
// Ctrl + Tab で最近使用した順にタブを切り替える を無效
user_pref("browser.ctrlTab.recentlyUsedOrder", false);


// ***************************** セキュリティ
// トラッキング拒否機能を有效
user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.trackingprotection.enabled", true);
// 暗号通貨マイニングをブロック
user_pref("privacy.trackingprotection.cryptomining.enabled", true);
// フィンガープリント採取をブロック
user_pref("privacy.trackingprotection.fingerprinting.enabled", true);
user_pref("privacy.resistFingerprinting", true); // フォント設定に影響
// パスワードマネージャを無效
user_pref("signon.rememberSignons", false);
// パスワードマネージャによる自動入力を無效
user_pref("signon.autofillForms", false);
// cookie のブロック
// 0: ブロックしない, 1: すべてのサードパーティ, 2: すべて, 3: 未訪問のウェブサイト, 4: サードパーティトラッカー
user_pref("network.cookie.cookieBehavior", 1);
// Firefox終了時にCookieとサイトデータを削除する
// 0: 削除しない, 2: 削除する
user_pref("network.cookie.lifetimePolicy", 2);
// 記憶させる履歷を詳細設定する
user_pref("privacy.history.custom", true);
user_pref("places.history.enabled", true); // 表示したページとファイルのダウンロードの履歴を残す
user_pref("browser.formfill.enable", false); // 検索やフォームの入力履歴を記憶させる
// 終了時に履歷を削除する
user_pref("privacy.sanitize.sanitizeOnShutdown", true);
user_pref("privacy.clearOnShutdown.cache", true);
user_pref("privacy.clearOnShutdown.cookies", true);
user_pref("privacy.clearOnShutdown.downloads", true);
user_pref("privacy.clearOnShutdown.formdata", true);
user_pref("privacy.clearOnShutdown.history", true);
user_pref("privacy.clearOnShutdown.offlineApps", true);
user_pref("privacy.clearOnShutdown.sessions", true); // 現在のログイン情報
user_pref("privacy.clearOnShutdown.siteSettings", false);
// 位置情報へのアクセスの要求をブロック
// 0: ブロックしない, 2: ブロックする
user_pref("permissions.default.geo", 2);
// カメラへのアクセスの要求をブロック
// 0: ブロックしない, 2: ブロックする
user_pref("permissions.default.camera", 2);
// マイクへのアクセスの要求をブロック
// 0: ブロックしない, 2: ブロックする
user_pref("permissions.default.microphone", 2);
// 通知の許可の要求をブロック
// 0: ブロックしない, 2: ブロックする
user_pref("permissions.default.desktop-notification", 2);
// 音声を自動再生するウェブサイトをブロックする
// 0: ブロックしない, 1: ブロックする
user_pref("media.autoplay.default", 1);
// VR デバイスへの要求をブロック
// 0: ブロックしない, 2: ブロックする
user_pref("permissions.default.xr", 2);
// アクセシビリティサービスによるブラウザーへのアクセスを止める
// 0: 止めない, 1: 止める
user_pref("accessibility.force_disabled", 1);
// 異なるドメイン間でのトラッキングを防止
user_pref("privacy.firstparty.isolate", true);
// セッションに關する追加情報（フォームの內容、スクロールバーの位置、Cookie、POST データ）を保存するか
// 0: どのサイトでも保存, 1: 暗號化されていないサイトに對してのみ保存, 2: 保存しない
user_pref("browser.sessionstore.privacy_level", 2);
// オートコンプリートのプレロード を無效
user_pref("browser.urlbar.speculativeConnect.enabled", false);
// Web サイトにマイクやカメラの狀態を把握させない
user_pref("media.navigator.enabled", false);


// ***************************** 警吿各種
// タブ開閉時の警吿を無效
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.tabs.warnOnCloseOtherTabs", false);
user_pref("browser.tabs.warnOnOpen", false);
// 「このページから移動しますか？」を無效
user_pref("dom.disable_beforeunload", true);
// about:config 起動時の警吿を無效
user_pref("general.warnOnAboutConfig", false);
user_pref("browser.aboutConfig.showWarning", false);
// HTML5動畫フルスクリーン時の警吿を無效
user_pref("full-screen-api.warning.timeout", 0);


// ***************************** Firefox システム
// Firefox のレポートを無效
user_pref("breakpad.reportURL", "");
user_pref("datareporting.healthreport.uploadEnabled", false); // Firefox が技術的な対話データを Mozilla へ送信することを許可する
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("app.shield.optoutstudies.enabled", false); // Firefox に調査のインストールと実行を許可する
// クラッシュレポートを無效
user_pref("browser.crashReports.unsubmittedCheck.enabled", false);
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit2", false); // Firefox があなたに代わって未送信のクラッシュレポートを送信することを許可する
// 安全な接續ではない警吿時のレポートを無效
user_pref("security.ssl.errorReporting.automatic", false);
user_pref("security.ssl.errorReporting.enabled", false);
user_pref("security.ssl.errorReporting.url", "");
// Google データベースへの照合關聯を無效
user_pref("browser.safebrowsing.provider.google.gethashURL", "");
user_pref("browser.safebrowsing.provider.google.lists", "");
user_pref("browser.safebrowsing.provider.google.reportMalwareMistakeURL", "");
user_pref("browser.safebrowsing.provider.google.reportPhishMistakeURL", "");
user_pref("browser.safebrowsing.provider.google.reportURL", "");
user_pref("browser.safebrowsing.provider.google.updateURL", "");
user_pref("browser.safebrowsing.provider.google4.dataSharing.enabled", false);
user_pref("browser.safebrowsing.provider.google4.dataSharingURL", "");
user_pref("browser.safebrowsing.provider.google4.gethashURL", "");
user_pref("browser.safebrowsing.provider.google4.lists", "");
user_pref("browser.safebrowsing.provider.google4.reportMalwareMistakeURL", "");
user_pref("browser.safebrowsing.provider.google4.reportPhishMistakeURL", "");
user_pref("browser.safebrowsing.provider.google4.reportURL", "");
user_pref("browser.safebrowsing.provider.google4.updateURL", "");
user_pref("browser.safebrowsing.downloads.enabled", false); // 危険なファイルのダウンロードをブロックする
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false); // 不要な危険ソフトウェアを警告する
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false); // 不要な危険ソフトウェアを警告する
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.malware.enabled", false); // 危険な詐欺コンテンツをブロックする
user_pref("browser.safebrowsing.phishing.enabled", false); // 危険な詐欺コンテンツをブロックする
user_pref("browser.safebrowsing.reportPhishURL", "");
user_pref("media.eme.enabled", false); // DRM 制御のコンテンツを再生 を無效
user_pref("media.gmp-widevinecdm.enabled", false); // DRM 制御のコンテンツを再生 を無效
// 既定ブラウザの確認を無效
user_pref("browser.shell.checkDefaultBrowser", false);
// 擴張機能インストールまでの待ち時間
user_pref("security.dialog_enable_delay", 0);
// 更新インストールのバックグラウンドサービスを無效
user_pref("app.update.service.enabled", false);
// 檢索エンジンの自動更新を無效
user_pref("browser.search.update", false);
// 推奨のパフォーマンス設定を使用する を無效
user_pref("browser.preferences.defaultPerformanceSettings.enabled", false);
// ハードウェアアクセラレーション機能を使用する
user_pref("layers.acceleration.disabled", false);
// WebRender を有效（使用にはハードウェアアクセラレーション機能が有效になつてゐる事）
user_pref("gfx.webrender.all", false);
// ブックマーク追加時にエディターを表示する を無效
user_pref("browser.bookmarks.editDialog.showForNewBookmarks", false);
// ダウンロードボタンを自動的に隱す
user_pref("browser.download.autohideButton", true);
// ダウンロード完了後のアニメーションを無效
user_pref("browser.download.animateNotifications", false);
// userChrome.css と userContent.css を有效
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);


// ***************************** 外部ソフトウェア
// MacType との聯繫
user_pref("gfx.content.azure.backends", "direct2d1.1,cairo");
